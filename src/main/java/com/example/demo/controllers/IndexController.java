package com.example.demo.controllers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.demo.models.Usuario;

@Controller
@RequestMapping("/app")
public class IndexController {
	// metodos de accion
	
	//@RequestMapping(value="/index", method=RequestMethod.GET)
	//@PostMapping(value="/index")
	@GetMapping({"/index","/","/home"})
	
	public String index(Model model) {
		model.addAttribute("titulo", "hola Spring Framework con Model!");
		return "index";
	}
	
	@GetMapping({"/perfil"})
	public String perfil(Model model) {
		Usuario usuario = new Usuario();
		usuario.setNombre("José");
		usuario.setApellido("Florián");
		usuario.setEmail("cjfn10101@gmail.com");
		model.addAttribute("usuario", usuario);
		model.addAttribute("titulo", "Perfil del usuario: ".concat(usuario.getNombre()));
		return "perfil";
	}
	
	//@RequestMapping("/lista")
	@GetMapping({"/lista"})
	public String lista(Model model) {
		
		List<Usuario> usuarios = new ArrayList<>();
		usuarios.add(new Usuario("Jose","Florian","cjfn10101@gmail.com"));
		usuarios.add(new Usuario("Nancy","Marroquin","nmarroquin@gmail.com"));
		usuarios.add(new Usuario("Ana","Florian","aflorian@gmail.com"));
		
		model.addAttribute("titulo", "Listado de usuarios");
		model.addAttribute("usuarios", usuarios);
		
		return "lista";
	}
	/*
	@ModelAttribute("usuarios")
	public List<Usuario> poblarUsuarios(){
		List<Usuario> usuarios = Arrays.asList(new Usuario("Jose","Florian","cjfn10101@gmail.com"),
				new Usuario("Nancy","Marroquin","nmarroquin@gmail.com"),
				new Usuario("Carla","Sandoval","csandoval@gmail.com"),
				new Usuario("A","Florian","cjfn10101@gmail.com"));
	}
	*/
	
}
